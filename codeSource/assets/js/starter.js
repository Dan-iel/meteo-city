// Importing JavaScript
//
// You have two choices for including Bootstrap's JS files—the whole thing,
// or just the bits that you need.


// Option 1
//
// Import Bootstrap's bundle (all of Bootstrap's JS + Popper.js dependency)

// import "../../node_modules/bootstrap/dist/js/bootstrap.bundle.min.js";


// Option 2
//
// Import just what we need

// If you're importing tooltips or popovers, be sure to include our Popper.js dependency
// import "../../node_modules/popper.js/dist/popper.min.js";

import "../../node_modules/bootstrap/js/dist/util.js";
import "../../node_modules/bootstrap/js/dist/modal.js";

let known = [];
const apiKey = 'e61fdc5a611471ce8515836312689390';
const city = document.getElementById("city");
const cities = document.getElementById("cities");
const regEx = /^[a-zA-Z]?[a-zA-Z\s]*$/;
let key = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L',
  'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X',
  'Y', 'Z'];

function logKey(e) {
  const keyUp = event.key;
  if (e.code === "Enter" || e.which === 13) {
    f();
  } else if (key.includes(`${keyUp}`)) {
    getCity()
  }
}

function f() {
  if (city.value.match(regEx) && city.value.trim().length >= 2) {
    fetch('https://api.openweathermap.org/data/2.5/weather?q=' + city.value + '&appid=' + apiKey)
      .then(response => response.json())
      .then(data => {
        let temp = data.main.temp;
        let max = data.main.temp_max;
        let min = data.main.temp_min;
        let hum = data.main.humidity;
        let dir = data.wind.deg;
        let speed = data.wind.speed;
        let id = data.name;

        temp = Math.round((temp - 32) * (5 / 9));
        max = Math.round((max - 32) * (5 / 9));
        min = Math.round((min - 32) * (5 / 9));
        let meteo = [id, temp, max, min, hum, dir, speed];
        known.push(id);
        const result = document.getElementById('result');
        result.innerHTML = '';
        result.innerHTML += "<p> You are in " + id + " current temp is " + temp + " max-temp is " + max + " and min-temp is " + min + " humidity, wind speed and direction are " + hum + ",  " + speed + " and " + dir + " respectively. </p>"
      });
  } else {
    alert("veillez saisir un nom de ville avec que des lettres et d'espace, SVP!");
  }


}

function getCity() {

  fetch('https://geo.api.gouv.fr/communes?nom=' + city.value + '&fields=departement&boost=population&autocomplete=1&limit=7')
    .then(response => response.json())
    .then(data => {
      cities.innerHTML = '';
      for (let i = 0; i < data.length; ++i) {
        let select = '<option value="' + data[i].nom + '">'/*+data[i].nom+'</option>'*/;
        cities.innerHTML += select;
      }
    });
}


let sub = document.getElementById("search");
sub.onclick = f;
city.onkeyup = logKey;


/*---------------------------------------------gps--------------------------------------------*
function g() {
  let crd;
  let options = {
    enableHighAccuracy: true,
    timeout: 5000,
    maximumAge: 0
  };

  function success(pos) {
     crd = pos.coords;

    /console.log('Your current position is:');
    console.log(`Latitude : ${crd.latitude}`);
    console.log(`Longitude: ${crd.longitude}`);
    console.log(`More or less ${crd.accuracy} meters.`);/

  }
  console.log(crd);

  function error(err) {
    console.warn(`ERROR(${err.code}): ${err.message}`);
  }

  navigator.geolocation.getCurrentPosition(success, error, options);
fetch('https://api.openweathermap.org/data/2.5/weather?lat=' + crd.latitude +'&lon='+crd.longitude+'&appid='+apiKey)
.then(response => response.json())
    .then(data => {
      let temp = data.main.temp;
      let max = data.main.temp_max;
      let min = data.main.temp_min;
      let hum = data.main.humidity;
      let dir = data.wind.deg;
      let speed = data.wind.speed;
      let id = data.name;
      console.log(id, temp, max, min, hum, dir, speed)
      known.push(id);
});
}

const gps = document.getElementById('gps');
function displayRadioValue() {

    if(gps.checked)
      var pos = confirm("En acceptant on t'annonce toujours si non c'est juste cette fois!!");
    if (pos === true){
      console.log("ha")
    } else {
      g()
    }
}
gps.onclick = displayRadioValue


let pass, target, option;

function good(pos) {
  let crd = pos.coords;

  if (target.latitude === crd.latitude && target.longitude === crd.longitude) {
    console.log('Congratulations, you reached the target');
    navigator.geolocation.clearWatch(id);
  }
}

function bad(err) {
  console.warn('ERROR(' + err.code + '): ' + err.message);
}

target = {
  latitude : 0,
  longitude: 0
};

option = {
  enableHighAccuracy: false,
  timeout: 5000,
  maximumAge: 0
};

pass = navigator.geolocation.watchPosition(good, bad, option);---------end gps----*/


/*---------------------------------------------exo rev--------------------------------------*/
function bonjour(n) {
  console.log("Bonjour " + n);
}

const cool = document.getElementById('cool');
cool.addEventListener('click', function (e) {
  const n = document.getElementById('n');
  bonjour(n.value);
});
